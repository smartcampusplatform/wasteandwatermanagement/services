<?php 
	// Require headers
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age:3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

	// Include database and object files
	include_once '../config/database.php';
	include_once '../objects/smartbin.php';

	// Get database connection
	$database = new Database();
	$db = $database->getConnection();

	// Prepate smartbin object
	$smartbin = new Smartbin($db);

	// Get id of product to be edited
	$data = json_decode(file_get_contents("php://input"));

	// set ID property of smartbin to be edited
	$smartbin->id_bin = $data->id_bin;

	// set smartbin property values
	// $smartbin->lat = $data->lat;
	// $smartbin->lng = $data->lng;
	$smartbin->status = $data->status;

	// Update the product
	if ($smartbin->update()) {

		// set response code - 200 OK
		http_response_code(200);

		// tell the user
		echo json_encode(array("message" => "Smartbin was updated."));
	} else { //unable to update the smartbin
		// Set response code - 503 service unavailable
		http_response_code(503);

		//	Tell the user
		echo json_encode(array("message" => "unable to update Smartbin."));

	}
?>