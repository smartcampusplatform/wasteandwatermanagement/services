<?php 
	
	// required headers
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: GET");

	// database connection
	// included database and object files
	include_once '../config/database.php';
	include_once '../objects/pipe.php';

	// instatiate database and product object
	$database = new Database();
	$db = $database->getConnection();

	// initialize object
	$pipe = new Pipe($db);

	// read smartbin
	// query smartbin
	$stmt = $pipe->read();
	$num = $stmt->rowCount();

	// check if more than 0 record found
	if ($num > 0) {

		// smartbins array
		$pipes_arr = array();
		$pipes_arr["records"] = array();

		// retrieve out table contents in a loop using fetch
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			// extract the row
			extract($row);

			$pipe_item = array(
				"id_pipe" => $id_pipe,
				"location" => $location,
				"voltage" => $voltage,
			);

			array_push($pipes_arr["records"], $pipe_item);
		}

		// set response code - 200 OK
		http_response_code(200);

		// show smartbins data in json format
		echo json_encode($pipes_arr);
	}

?>