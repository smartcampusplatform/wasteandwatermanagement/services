<?php 
	
	// required headers
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: GET");

	// database connection
	// included database and object files
	include_once '../config/database.php';
	include_once '../objects/water.php';

	// instatiate database and product object
	$database = new Database();
	$db = $database->getConnection();

	// initialize object
	$water = new Water($db);

	// read smartbin
	// query smartbin
	$stmt = $water->read();
	$num = $stmt->rowCount();

	// check if more than 0 record found
	if ($num > 0) {

		// smartbins array
		$water_arr = array();
		$water_arr["records"] = array();

		// retrieve out table contents in a loop using fetch
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			// extract the row
			extract($row);

			$water_item = array(
				"id_water" => $id_water,
				"location" => $location,
				"voltage" => $voltage,
			);

			array_push($water_arr["records"], $water_item);
		}

		// set response code - 200 OK
		http_response_code(200);

		// show smartbins data in json format
		echo json_encode($water_arr);
	}

?>