<?php 

	class Smartbin {
		// database connection and table name
		private $conn;
		private $table_name = "smartbin";

		// object attributes
		public $id_bin;
		public $lat;
		public $lng;
		public $status;

		// constructor with $db as database connection
		public function __construct($db) {
			$this->conn = $db;
		}

		// read smartbins
		function read() {
			// select all query
			$query = "SELECT * FROM " . $this->table_name;
			// prepare query statement
			$stmt = $this->conn->prepare($query);

			// execute query
			$stmt->execute();

			return $stmt;		
		}

		// update function product
		function update() {

			// update query
			$query = "UPDATE smartbin 
			SET status = :status
			WHERE id_bin = :id_bin";

			// Prepare query statement
			$stmt = $this->conn->prepare($query);

			// Sanitize
			$this->id_bin=htmlspecialchars(strip_tags($this->id_bin));
		    $this->lat=htmlspecialchars(strip_tags($this->lat));
		    $this->lng=htmlspecialchars(strip_tags($this->lng));
		    $this->status=htmlspecialchars(strip_tags($this->status));

		    // Bind new values
		    $stmt->bindParam(':status', $this->status);
		    $stmt->bindParam(':id_bin', $this->id_bin);
		    // $stmt->bindParam(':lat', $this->lat);
		    // $stmt->bindParam(':lng', $this->lng);

		    // Execute the query
		    if ($stmt->execute()) {
		    	return true;
		    }

		    return false;
		}
	}
?>